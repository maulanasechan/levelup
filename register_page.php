<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="style.css">
  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <script src="main.js"></script>
  <title>Document</title>
</head>

<body>
  <div class="container">
    <div class="row justify-content-center loginpage">
      <div class="col-auto visi">
        <h3>LEVEL UP</h3>
        <div class="row">
          <div class="col-auto">
            <img src="images/icon/icons-1.png" class="icon" alt="">
          </div>
          <div class="col-auto">
            <h5>Terlengkap dan Terpercaya</h5>
            <p>Lebih dari ratusan produk dan penjual yang
              <br>telah berlangganan</p>
          </div>
        </div>
        <div class="row">
          <div class="col-auto">
            <img src="images/icon/icons-2.png" class="icon" alt="">
          </div>
          <div class="col-auto">
            <h5>Pengiriman Instan</h5>
            <p>Hanya dalam waktu kurang dari 5 menit
              <br>produk sampai</p>
          </div>
        </div>
        <div class="row">
          <div class="col-auto">
            <img src="images/icon/icons-3.png" class="icon" alt="">
          </div>
          <div class="col-auto">
            <h5>Produk Legal</h5>
            <p>Produk legal 100% sehingga akun aman
              <br>sentosa</p>
          </div>
        </div>
        <div class="row">
          <div class="col-auto">
            <img src="images/icon/icons-4.png" class="icon" alt="">
          </div>
          <div class="col-auto">
            <h5>Tempat Favorit Gamer</h5>
            <p>Ribuan gamer telah berbelanja dan
              <br>berlangganan di level up</p>
          </div>
        </div>
      </div>
      <div class="col-auto regist">
        <div class="box1">
          <h3>Sign Up</h3>
          <input class="input1" type="text" name="fullname" id="fullname" placeholder="&#xf007   Full Name">
          <input class="input3" type="text" name="username" id="username" placeholder="&#xf2bb   Username">
          <input class="input3" type="text" name="email" id="email" placeholder="&#xf0e0   Email">
          <div class="row2">
            <input class="input2" type="password" name="password" id="password" placeholder="&#xf084   Password">
            <button class="show" id="show" onclick="hideandshow()">show</button>
          </div>
          <div class="row2">
            <input class="input4" type="tel" name="phone" id="phone" placeholder="&#xf10b   Phone Number" pattern="[0-9]{11}">
            <select name="phoneid" id="phoneid" class="minimal">
              <option value="+62">+62</option>
              <option value="+12">+12</option>
            </select>
          </div>
          <ul>
            <li class="active"></li>
            <li></li>
            <li></li>
          </ul>
          <input type="submit" name="username" id="username" value="Next">
        </div>
        <div class="box2">
          <p>Don't have an account? <span>Sign Up</span></p>
        </div>
      </div>
    </div>
  </div>
</body>

</html>