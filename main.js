function hideandshow() {
  var button = document.getElementById("show");
  var show = document.getElementById("password");
  var flag = show.type;

  if (flag == "password") {
    button.style.backgroundColor = "#5BA27F";
    show.type = "text";
    button.innerHTML = "hide";
  } else {
    button.style.backgroundColor = "#828282";
    show.type = "password";
    button.innerHTML = "show";
  }
}